const http = require('http')
const app = require('../app.js')
const logger = require('../utils/logger')

const PORT = 8080
app.set('port', PORT)

const server = http.createServer(app)
server.listen(PORT)
server.on('error', () => console.log(error))
server.on('listening', () => logger.info(`Running on http://localhost:${PORT}`))
