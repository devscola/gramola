process.env.NODE_ENV = 'testing'
const request = require('supertest')
const app = require('../../app.js')
const SUCCESS = 200

describe('Slack command', () => {
  test('hello request returns world', async () => {
    const response = await request(app).post('/slack/hello')
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.text).toBe("hello world")
  })
})