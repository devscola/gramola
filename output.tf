output "user_data" {
  value =  data.template_file.init.rendered
}

output "server_public_ip" {
  value = hcloud_server.ubuntu.ipv4_address
}

output "server_ssh_username" {
  value = var.username
}