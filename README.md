# Gramola

Gramola es un proyecto para practicar y mejorar nuestro DevOps-fu.

Consiste en un interface para integrar listas de reproducción de música dentro de software de chat tipo Mattermost, Slack etc, es en resumen como si cada canal de chat tuviera su propia gramola.

En este proyecto usaremos la aproximación #NoCode para centrarnos 100% en la parte Backend + DevOps, es decir toda la parte UI/UX residirá dentro del software de chat, usaremos las características de interactividad que nos proporcionan.

Vamos a diseñar Gramola intentando aplicar al máximo el paradigma propuesto por "Cloud Native" siendo estas 4 de sus ideas principales:

- Contenedores
- Microservicios
- DevOps
- CI/CD

Queremos también diseñarlo desde el principio teniendo en cuenta la portabilidad desde un punto de vista DevOps, siendo lo más agnosticos posibles, sin casarnos con un proveedor de cloud concreto.

Por el momento estamos definiendo cómo vamos a funcionar, nos reuniremos todos los lunes de 18 a 19.

Tableros:
- https://cloud.devscola.org/apps/deck/#/board/32

### Flujo de trabajo

Es importante saber que en el flujo de trabajo de este equipo no se puede hacer _push_ directamente a _master_. Hace falta crear una rama y un _merge request_ que pase los test de _CI_ antes de integrarse con _master_. Las ramas deben tener un ciclo de vida muy corto, de como máximo dos días y además todas deben partir de la rama _master_. En los _merge request_ no se no se harán comentarios bloqueantes a otros desarrolladores, su única finalidad es evitar que los test en _master_ fallen y una vez se acepte el _MR_ la rama debería borrarse. Los _MR_ no deben bloquear el flujo de trabajo, para mejorar el código se usarán concerns. Un ejemplo de flujo de trabajo con el repositorio sería el siguiente:

1. Hago _pull_ de _master_
2. Creo la rama en la que quiero trabajar
3. Edito el código
4. Hago _commit_ en la rama
5. Hago un _push_ de mi rama ```git push -u origin <mi_ramita>```
6. Creo un _merge request_ desde mi rama hacia _master_ (después de hacer _push_, Gitlab nos facilita un enlace muy cómodo para esto)
7. Una vez han pasado los _tests_ de esa rama, clico en el botón de _merge_ y mi código se integra con _master_


### Entorno de desarrollo (Vagrant + Docker)

Requisitos previos:

- Instalar [Vagrant](https://www.vagrantup.com/docs/installation)
- Instalar [VirtualBox](https://www.virtualbox.org/)


Para arrancar la máquina Vagrant utilizamos el comando ```vagrant up```

A continuación para acceder a la máquina en si utilizamos el comando ```vagrant ssh```

